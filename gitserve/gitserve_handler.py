#!/usr/bin/env python3

import codecs
import hashlib
import zlib

from abc import ABC, abstractmethod
from http import HTTPStatus
from http.server import BaseHTTPRequestHandler
from dataclasses import dataclass
from typing import Union, List, Optional
from enum import Enum


@dataclass
class Config:
    branch_name: str

    @staticmethod
    def default():
        return Config(branch_name="main")
    
    def get_head_ref(self) -> str:
        return f"refs/heads/{self.branch_name}"

class GitObjectType(Enum):
    COMMIT = 'commit'
    TREE = 'tree'
    BLOB = 'blob'

class GitObject(ABC):

    @abstractmethod
    def get_type(self) -> GitObjectType:
        pass

    def serialize(self) -> bytes:
        contents_bin = self.serialize_raw()
        type_bin = self.get_type().value.encode("utf-8")
        return type_bin + b' ' + str(len(contents_bin)).encode("utf-8") + b'\0' + contents_bin

    def serialize_and_hash_hex(self, hasher = hashlib.sha1) -> (bytes, str):
        serialized = self.serialize()
        hash = hasher(serialized).hexdigest()
        return (serialized, hash)

    @abstractmethod
    def serialize_raw(self) -> bytes:
        pass

    def hash_hex(self, hasher = hashlib.sha1) -> str:
        return hasher(self.serialize()).hexdigest()

    def hash(self, hasher = hashlib.sha1) -> bytes:
        return hasher(self.serialize()).digest()

@dataclass
class CommitObject(GitObject):
    tree_hash: str
    parent_hash: Optional[str] = None
    author: str = "Anonymous <anonymous@example.com>"
    timestamp: int = 0
    offset: str = "+0000"
    commit_message: str = "Initial Commit"

    def get_type(self) -> GitObjectType:
        return GitObjectType.COMMIT
    
    def serialize_raw(self) -> bytes:
        contents = []
        contents.append(f"tree {self.tree_hash}")
        if self.parent_hash:
            contents.append(f"parent {self.parent_hash}")
        contents.append(f"author {self.author} {self.timestamp} {self.offset}")
        contents.append(f"committer {self.author} {self.timestamp} {self.offset}")
        contents.append("")
        contents.append(self.commit_message)
        contents.append("")
        contents = "\n".join(contents)
        return contents.encode("utf-8")

class TreeType(Enum):
    FILE = "100644"
    EXE_FILE = "100755"
    DIR = "040000"
    SYM_LINK = "120000"

@dataclass
class TreeEntry:
    type: TreeType
    name: str
    blob_hash: str

@dataclass
class TreeObject(GitObject):
    entries: List[TreeEntry]

    def get_type(self) -> GitObjectType:
        return GitObjectType.TREE
    
    def serialize_raw(self) -> bytes:
        contents = []
        for entry in self.entries:
            entry_line = f"{entry.type.value} {entry.name}"
            contents.append(entry_line.encode() + b'\0' + codecs.decode(entry.blob_hash, "hex_codec"))

        return b''.join(contents)

@dataclass
class BlobObject(GitObject):
    contents: bytes

    def get_type(self) -> GitObjectType:
        return GitObjectType.BLOB

    def serialize_raw(self) -> bytes:
        return self.contents

@dataclass
class TlobObject(GitObject):
    contents: str

    def get_type(self) -> GitObjectType:
        return GitObjectType.BLOB

    def serialize_raw(self) -> bytes:
        return self.contents.encode("utf-8")

class VirtualRepo(ABC):
    
    @abstractmethod
    def get_main_commit_hash(self) -> str:
        pass

    @abstractmethod
    def get_object_from_hash(self, hash: str) -> Union[bytes, GitObject]:
        pass

class GitRequestHandler(BaseHTTPRequestHandler):

    def __init__(self, *args, repo: VirtualRepo = None, config: Config = None, **kwargs):
        self.repo = repo
        self.config = config or Config.default()
        super().__init__(*args, **kwargs)

    def do_GET(self):
        if self.path.startswith("/objects"):
            self._serve_object()
        elif self.path.startswith("/info/refs"):
            self._serve_info_refs()
        elif self.path.startswith("/HEAD"):
            self._serve_head()
        else:
            self._answer_with(status= HTTPStatus.NOT_FOUND)
    
    def _serve_info_refs(self):
        self._answer_with(f"{self.repo.get_main_commit_hash()}\t{self.config.get_head_ref()}\n")

    def _serve_head(self):
        self._answer_with(f"ref: {self.config.get_head_ref()}\n")

    def _serve_object(self):
        object_id = self.path[len("/objects/"):].replace("/", "")
        object = self.repo.get_object_from_hash(object_id)
        if isinstance(object, GitObject):
            object = object.serialize()
        self._answer_with(content=zlib.compress(object))

    def _answer_with(self, content: Union[bytes, str] = None, status: HTTPStatus = HTTPStatus.OK, content_type: str = None):
        self.send_response(status)

        if content_type is None and content is not None:
            if type(content) == str:
                content_type = "text/plain; charset=utf-8"
            else:
                content_type = "application/octet-stream"

        if content_type is not None:
            self.send_header("Content-type", content_type)

        if type(content) == str:
            content = content.encode()

        if content is not None:
            self.send_header("Content-length", len(content))
        self.end_headers()

        if content is not None:
            self.wfile.write(content)
