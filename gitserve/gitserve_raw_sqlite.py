#!/usr/bin/env python3

import sqlite3
from gitserve import GitObject, CommitObject, VirtualRepo
from contextlib import closing
from typing import Union

class RawSqliteGitTreeBuilder:

    def __init__(self, conn: sqlite3.Connection) -> None:
        self.conn = conn

    def init(self, force=False):
        with closing(self.conn.cursor()) as cursor:
            if force:
                cursor.execute("DROP TABLE IF EXISTS object")
                cursor.execute("DROP TABLE IF EXISTS head")
            
            cursor.execute("CREATE TABLE head(commit_hash TEXT)")
            cursor.execute("CREATE TABLE object(hash TEXT PRIMARY KEY, content BLOB)")
            self.conn.commit()

    def add(self, object: GitObject):
        with closing(self.conn.cursor()) as cursor:
            serialized, hash_hex = object.serialize_and_hash_hex()
            cursor.execute("INSERT INTO object (hash, content) VALUES (?, ?)", (hash_hex, serialized))
            self.conn.commit()

    def set_head_commit(self, commit: CommitObject):
        with closing(self.conn.cursor()) as cursor:
            cursor.execute("DELETE FROM head")
            cursor.execute("INSERT INTO head(commit_hash) VALUES (?)", (commit.hash_hex(),))
            self.conn.commit()

class RawSqliteRepo(VirtualRepo):

    def __init__(self, conn: sqlite3.Connection) -> None:
        self.conn = conn

    def get_main_commit_hash(self) -> str:
        with closing(self.conn.cursor()) as cursor:
            cursor.execute("SELECT commit_hash FROM head LIMIT 1")
            return cursor.fetchone()[0]

    def get_object_from_hash(self, hash: str) -> Union[bytes, GitObject]:
        with closing(self.conn.cursor()) as cursor:
            cursor.execute("SELECT content FROM object where hash = ? LIMIT 1", (hash,))
            return cursor.fetchone()[0]
