#!/usr/bin/env python3

import codecs
import hashlib
import sqlite3
import zlib
import zipfile

from contextlib import closing
from typing import List, Union
from gitserve import BlobObject, GitObject, TreeType, GitObjectType, TreeEntry, TreeObject, CommitObject, VirtualRepo
from abc import ABC, abstractmethod

class VirtualObjectHandler(ABC):

    @abstractmethod
    def decode(self, value: bytes) -> bytes:
        pass

class SqliteGitTreeBuilder:

    def __init__(self, conn: sqlite3.Connection) -> None:
        self.conn = conn

    def init(self, force=False):
        with closing(self.conn.cursor()) as cursor:
            if force:
                cursor.execute("DROP TABLE IF EXISTS object")
                cursor.execute("DROP TABLE IF EXISTS \"commit\"")
                cursor.execute("DROP TABLE IF EXISTS tree")
                cursor.execute("DROP TABLE IF EXISTS blob")
                cursor.execute("DROP TABLE IF EXISTS head")
            
            cursor.execute("CREATE TABLE head(commit_hash TEXT)")
            cursor.execute("CREATE TABLE \"commit\"(hash TEXT PRIMARY KEY, tree_hash TEXT, parent_hash TEXT, author TEXT, timestamp INTEGER, offset TEXT, message TEXT)")
            cursor.execute("CREATE TABLE tree(hash TEXT, type TEXT, blob_hash TEXT, name TEXT, PRIMARY KEY(hash, name))")
            cursor.execute("CREATE TABLE blob(hash TEXT PRIMARY KEY, value BLOB)")
            cursor.execute("CREATE TABLE object(hash TEXT PRIMARY KEY, type TEXT)")
            self.conn.commit()

    def add_file(self, path: str, contents: GitObject):
        elements = path.split("/")

        with closing(self.conn.cursor()) as cursor:
            self._add_file(cursor, path, elements, [""], contents)

        self.conn.commit()

    def _add_file(self, cursor: sqlite3.Cursor, path: str, elements: List[str], cur_dirs: List[str], contents: BlobObject) -> int:
        if len(elements) == 1:
            file_name = elements[0]

            hash_hex = contents.hash_hex()
            serialized_raw = contents.serialize_raw()

            compressed = zlib.compress(serialized_raw)

            cursor.execute("INSERT OR IGNORE INTO blob(hash, value) VALUES(?, ?)", (hash_hex, compressed))
            cursor.execute("INSERT OR IGNORE INTO object(hash, type) VALUES(?, ?)", (hash_hex, GitObjectType.BLOB.name))

            return self._insert_file(cursor, file_name, cur_dirs, hash_hex)

        else:
            first = elements[0]
            tail = elements[1:]

            dir_id = self._add_file(cursor, path, tail, cur_dirs + [first], contents)
            return self._insert_file(cursor, first, cur_dirs, dir_id)

    def _insert_file(self, cursor: sqlite3.Cursor, file_name: str, cur_dirs: [str], value: Union[str, int]) -> int:
        cur_dir = "/".join(cur_dirs)

        cursor.execute("INSERT OR IGNORE INTO tree_ids (path, depth) VALUES (?, ?)", (cur_dir, len(cur_dirs)))
        cursor.execute("SELECT id FROM tree_ids WHERE path = ?", (cur_dir,))
        tree_id = cursor.fetchone()[0]

        if type(value) == str:
            type_tag = TreeType.FILE.name
        else:
            type_tag = TreeType.DIR.name

        cursor.execute("INSERT OR IGNORE INTO tree_mod(id, type, dest, name) VALUES (?, ?, ?, ?)", (tree_id, type_tag, value, file_name))
        
        return tree_id

    def start_commit(self):
        # todo: make sure there is no current commit
        with closing(self.conn.cursor()) as cursor:
            cursor.execute("DROP TABLE IF EXISTS tree_mod")
            cursor.execute("DROP TABLE IF EXISTS tree_ids")
        
            cursor.execute("CREATE TABLE tree_mod(id INTEGER, type TEXT, dest TEXT, name TEXT, PRIMARY KEY(id, name))")
            cursor.execute("CREATE TABLE tree_ids(id INTEGER PRIMARY KEY, path TEXT UNIQUE, depth INTEGER, hash TEXT)")
            self.conn.commit()

    def build_commit(self,
                     parent_hash: str = None,
                     author: str = None,
                     timestamp: int = None,
                     offset: str = None,
                     message: str = None,
                     ) -> str:
        # todo: make sure commit is in progress
        with closing(self.conn.cursor()) as cursor:
            with closing(self.conn.cursor()) as tree_cursor, closing(self.conn.cursor()) as tree_hash_cursor:
                for row in cursor.execute("SELECT id FROM tree_ids ORDER BY depth DESC"):
                    tree_id = row[0]
                    tree_entries = []
                    for dir_row in tree_cursor.execute("SELECT type, dest, name FROM tree_mod WHERE id = ?", (tree_id,)):
                        tree_type = TreeType[dir_row[0]]
                        dest = dir_row[1]

                        if tree_type == TreeType.DIR:
                            tree_hash_cursor.execute("SELECT hash FROM tree_ids WHERE id = ?", (dest,))
                            dest = tree_hash_cursor.fetchone()[0]

                        tree_entries.append(TreeEntry(
                            type = tree_type,
                            name = dir_row[2],
                            blob_hash = dest,
                        ))
                    
                    tree = TreeObject(tree_entries)
                    tree_hash = tree.hash_hex()
                    for entry in tree.entries:
                        tree_cursor.execute("INSERT OR IGNORE INTO tree(hash, type, blob_hash, name) VALUES(?, ?, ?, ?)", (tree_hash, entry.type.name, entry.blob_hash, entry.name))
                        tree_cursor.execute("INSERT OR IGNORE INTO object(hash, type) VALUES(?, ?)", (tree_hash, GitObjectType.TREE.name))
                        tree_cursor.execute("UPDATE tree_ids SET hash = ? WHERE id = ?", (tree_hash, tree_id))
            
            commit_obj = CommitObject(tree_hash)
            if parent_hash:
                commit_obj.parent_hash = parent_hash
            if author:
                commit_obj.author = author
            if timestamp:
                commit_obj.timestamp = timestamp
            if offset:
                commit_obj.offset = offset
            if message:
                commit_obj,message = message
            commit_hash = commit_obj.hash_hex()
            cursor.execute("INSERT INTO \"commit\"(hash, tree_hash, author, timestamp, offset, message) VALUES(?, ?, ?, ?, ?, ?)", (commit_hash, commit_obj.tree_hash, commit_obj.author, commit_obj.timestamp, commit_obj.offset, commit_obj.commit_message))
            cursor.execute("INSERT OR IGNORE INTO object(hash, type) VALUES(?, ?)", (commit_hash, GitObjectType.COMMIT.name))
            cursor.execute("DELETE FROM head")
            cursor.execute("INSERT INTO head(commit_hash) VALUES (?)", (commit_hash,))

            cursor.execute("DROP TABLE tree_ids")
            cursor.execute("DROP TABLE tree_mod")
            self.conn.commit()

            return commit_hash

class SqliteRepo(VirtualRepo):

    def __init__(self, conn: sqlite3.Connection, virual_object_handler: VirtualObjectHandler = None) -> None:
        self.conn = conn
        self.virtual_object_handler = virual_object_handler
    
    def get_main_commit_hash(self) -> str:
        with closing(self.conn.cursor()) as cursor:
            cursor.execute("SELECT commit_hash FROM head LIMIT 1")
            return cursor.fetchone()[0]

    def get_object_from_hash(self, hash: str) -> Union[bytes, GitObject]:
        with closing(self.conn.cursor()) as cursor:
            cursor.execute("SELECT type FROM object WHERE hash = ?", (hash,))
            object_type = GitObjectType[cursor.fetchone()[0]]

            if object_type == GitObjectType.BLOB:
                cursor.execute("SELECT value FROM blob WHERE hash = ?", (hash,))
                blob_value_compressed = cursor.fetchone()[0]
                blob_value = zlib.decompress(blob_value_compressed)
                if self.virtual_object_handler:
                    blob_value = self.virtual_object_handler.decode(blob_value)
                return BlobObject(blob_value)

            elif object_type == GitObjectType.TREE:
                tree_entries = []
                for entry in cursor.execute("SELECT type, blob_hash, name FROM tree WHERE hash = ?", (hash,)):
                    tree_entries.append(TreeEntry(
                        type = TreeType[entry[0]],
                        name = entry[2],
                        blob_hash = entry[1],
                        ))
                return TreeObject(tree_entries)

            elif object_type == GitObjectType.COMMIT:
                cursor.execute("SELECT tree_hash, parent_hash, author, timestamp, offset, message FROM \"commit\" WHERE hash = ?", (hash,))
                commit_data = cursor.fetchone()
                return CommitObject(
                    tree_hash = commit_data[0],
                    parent_hash = commit_data[1],
                    author = commit_data[2],
                    timestamp=commit_data[3],
                    offset=commit_data[4],
                    commit_message=commit_data[5],
                )

class ZipBlobObject(GitObject):
    def __init__(self, zipfile: zipfile.ZipFile, info: zipfile.ZipInfo) -> None:
        self.zipfile = zipfile
        self.info = info

    def hash_hex(self, hasher=hashlib.sha1) -> str:
        h = hasher(b'blob ' + str(self.info.file_size).encode("utf-8") + b'\0')
        with self.zipfile.open(self.info) as f:
            while True:
                chunk = f.read(4096)
                if not chunk:
                    break
                h.update(chunk)
        return h.hexdigest()
    
    def serialize_raw(self) -> bytes:
        return self.info.filename.encode()
    
    def get_type(self) -> GitObjectType:
        return GitObjectType.BLOB

class ZipVirtualObjectHandler(VirtualObjectHandler):
    def __init__(self, zipfile: zipfile.ZipFile) -> None:
        self.zipfile = zipfile

    def decode(self, value: bytes) -> bytes:
        path = value.decode()
        with self.zipfile.open(path) as f:
            return f.read()
