#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name = "gitserve",
    version = "0.0.2",
    packages = find_packages(),
    description = "utilities for serving anything as a gitrepo",
    author="Alexander Weld",
    author_email="weldale@gmail.com",
    license="MIT",
)
