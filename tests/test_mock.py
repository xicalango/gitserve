#!/usr/bin/env python3

from typing import Union
from functools import partial
from http.server import HTTPServer
from .context import gitserve
from gitserve import *

class MockRepo(VirtualRepo):

    MY_BLOB = TlobObject("Hallo Weld!")

    def _get_main_commit(self) -> CommitObject:
        return CommitObject(
                tree_hash=self._get_main_tree().hash_hex(), 
                author="Alexander Weld <weldale@gmail.com>",
                timestamp=1590183524,
                offset="+0200",
                commit_message="added public keys",
                )

    def _get_main_tree(self) -> TreeObject:
        return TreeObject(
            entries = [
                TreeEntry(
                    type=TreeType.FILE,
                    name="hallo_weld.txt",
                    blob_hash=MockRepo.MY_BLOB.hash_hex(),
                ),
                TreeEntry(
                    type=TreeType.FILE,
                    name="hallo_weld2.txt",
                    blob_hash=MockRepo.MY_BLOB.hash_hex(),
                ),
            ]
        )

    def get_main_commit_hash(self) -> str:
        return self._get_main_commit().hash_hex()
    
    def get_object_from_hash(self, hash: str) -> Union[bytes, GitObject]:
        if hash == self.get_main_commit_hash():
            return self._get_main_commit()
        elif hash == self._get_main_tree().hash_hex():
            return self._get_main_tree()
        elif hash == MockRepo.MY_BLOB.hash_hex():
            return MockRepo.MY_BLOB
        else:
            raise ValueError(f"unknown hash: {hash}")
    

def main():
    server_address=('', 8000)
    handler_class=partial(GitRequestHandler, repo = MockRepo())
    httpd = HTTPServer(server_address, handler_class)
    print(f"serving at {server_address}")
    httpd.serve_forever()

if __name__ == "__main__":
    main()
