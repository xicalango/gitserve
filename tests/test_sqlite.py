import sqlite3
import zipfile

from functools import partial
from http.server import HTTPServer
from .context import gitserve
from gitserve import *

def serve(conn: sqlite3.Connection, virtual_object_handler: VirtualObjectHandler = None):

    server_address=('', 8000)
    handler_class=partial(GitRequestHandler, repo = SqliteRepo(conn, virual_object_handler=virtual_object_handler))
    httpd = HTTPServer(server_address, handler_class)
    print(f"serving at {server_address}")
    httpd.serve_forever()


def main():
    import time

    conn = sqlite3.connect(":memory:")


    gen = SqliteGitTreeBuilder(conn)
    gen.init(force=True)
    gen.start_commit()

    with zipfile.ZipFile("test.zip") as f:
        for entry in f.infolist():
            if entry.is_dir():
                continue
            gen.add_file(entry.filename, ZipBlobObject(f, entry))

        gen.build_commit(timestamp=int(time.time()))

        vzf = ZipVirtualObjectHandler(f)
        serve(conn, vzf)

    conn.close()

if __name__ == "__main__":
    main()
