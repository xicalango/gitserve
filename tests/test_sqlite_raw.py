#!/usr/bin/env python3

import sqlite3

from functools import partial
from http.server import HTTPServer
from .context import gitserve
from gitserve import *

if __name__ == "__main__":
    conn = sqlite3.connect(":memory:")
    builder = RawSqliteGitTreeBuilder(conn)
    builder.init(force=True)
    hallo_weld_object = TlobObject("hallo weld")
    tree = TreeObject([
        TreeEntry(
            type=TreeType.FILE,
            name="hallo_weld.txt",
            blob_hash=hallo_weld_object.hash_hex(),
        )
    ])
    commit = CommitObject(
        tree_hash=tree.hash_hex()
    )
    commit2 = CommitObject(
        tree_hash=tree.hash_hex(),
        parent_hash=commit.hash_hex(),
        commit_message="2nd commit"
    )
    builder.add(hallo_weld_object)
    builder.add(tree)
    builder.add(commit)
    builder.add(commit2)
    builder.set_head_commit(commit2)

    server_address=('', 8000)
    handler_class=partial(GitRequestHandler, repo = RawSqliteRepo(conn))
    httpd = HTTPServer(server_address, handler_class)
    print(f"serving at {server_address}")
    httpd.serve_forever()
