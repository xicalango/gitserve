#!/usr/bin/env python3

import zlib
from argparse import ArgumentParser, FileType

parser = ArgumentParser()
parser.add_argument("input_file", type=FileType("rb"))
parser.add_argument("output_file", type=FileType("wb"))
args = parser.parse_args()

contents = args.input_file.read()
decompressed = zlib.decompress(contents)
args.output_file.write(decompressed)

args.input_file.close()
args.output_file.close()
